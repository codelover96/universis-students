import { Component, OnInit } from '@angular/core';
import { RequestsService} from '../../services/requests.service';
import {AngularDataContext} from '@themost/angular';
import {LoadingService} from '../../../shared/services/loading.service';
import {ModalService} from '../../../shared/services/modal.service';
import {TranslateService} from '@ngx-translate/core';
import {translateType} from '@angular/compiler-cli/src/ngtsc/transform/src/translator';


@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.scss'],
  providers: [ RequestsService]
})
export class RequestsListComponent implements OnInit {

  public myModal;
  public largeModal;
  public smallModal;
  public primaryModal;
  public successModal;
  public warningModal;
  public dangerModal;
  public infoModal;
  public doctype: any;
  public activerequests: any;
  public documentrequests: any = [];
  public messagerequests: any [];
  public allrequests: any = [];
  public requests: any;
  public checkrequests: any;


  constructor(private _context: AngularDataContext, private translate: TranslateService, private loadingService: LoadingService, private  requestsService: RequestsService, private modalService: ModalService) {}

  ngOnInit() {
// show loading
    this.loadingService.showLoading();
    this.requestsService.getDocumentTypes().then((res) => {
      this.doctype = res.value;
    });
    this.requestsService.getActiveRequests().then((res) => {
      this.activerequests = res.value;
    });

    this.requestsService.getDocumentRequests().then((res) => {
      this.documentrequests = res.value;
      this.requestsService.getMessageRequests().then((res) => {
        this.messagerequests = res.value;
        this.allrequests = this.documentrequests.concat(this.messagerequests);
        this.allrequests.sort((a, b) => {
          return b.dateCreated - a.dateCreated;
        });
      });
// hide loading
      this.loadingService.hideLoading();
    });
  }

  sendRequest(title: string) {
    return this._context.model('RequestDocumentActions').save({
      object: {
        alternateName: title
      },
      // description: description,
    })
      .then(() => {
        window.location.href = ('/#/requests/list');
        window.location.reload(true);
      }).catch((err) => {
        console.log(Error);
      });
  }

  OpenDialog() { this.modalService.openDialog( this.translate.instant('Requests.Attention'), this.translate.instant('Requests.AlreadyExists') ,true );
  }
  checkActiveRequests (requests) {
    this.checkrequests = requests;
    let j = 0;
    for (let i = 0; i < this.activerequests.length; i++) {
      if (this.checkrequests === this.activerequests[i].alternateName) {
        j++;
      }
    }
    if (j === 0) { return this.sendRequest(this.checkrequests); } else { return this.OpenDialog(); }
  }
}
