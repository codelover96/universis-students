import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService} from '../../shared/services/configuration.service';



@Injectable()
export class RequestsService {

  constructor(private _context: AngularDataContext, private _configuration: ConfigurationService) {
    //
  }

   getDocumentTypes() {
    return this._context.model('DocumentConfigurations')
      .asQueryable()
      .where('inLanguage')
      .equal(this._configuration.getCurrentLang())
      .getItems();
   }
  getActiveRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .where('actionStatus/alternateName')
      .equal('ActiveActionStatus')
      .select('object/name as requestActionName', 'object/alternateName as alternateName',
          'actionStatus/alternateName as status', 'description', 'dateCreated')
      .expand('actionStatus', 'object')
      .getItems();
  }

  getDocumentRequests() {
    return this._context.model('RequestDocumentActions')
      .asQueryable()
      .select('object/name as requestActionName', 'object/alternateName as alternateName',
          'actionStatus/alternateName as status', 'description', 'dateCreated')
      .expand('object', 'actionStatus')
      .orderByDescending('dateCreated')
      .getItems();
  }
  getMessageRequests() {
    return this._context.model('RequestMessageActions')
      .asQueryable()
     .select('name as requestActionName', 'actionStatus/alternateName as status', 'description', 'dateCreated')
      .expand('actionStatus')
      .orderByDescending('dateCreated')
      .getItems();
  }
}
