import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent {
  /*
     if is true it will shown component progress-bar-semester
     if is false it will shown component progress-bar-degree
    */
  public viewsemester = false;

  constructor() { }

}
