import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import {ErrorService} from '../error/error.service';

@Component({
  selector: 'auth-callback',
  template: '<div></div>',
  encapsulation: ViewEncapsulation.None,
})

export class AuthCallbackComponent implements OnInit {

  constructor(
    private _router: Router,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _errorService: ErrorService,
    private _authService: AuthenticationService) {
  }

  ngOnInit() {
    // validate code
    this._route.queryParamMap.subscribe((paramMap: ParamMap) => {
      this._authService.callback(paramMap.get('access_token')).subscribe((res) => {
        if (typeof res === 'object') {
          this._router.navigate(['/']);
        }
      }, (err) => {
        return this._errorService.navigateToError(err);
      });
    });
  }

}
