import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { ProfileService } from '../../../profile/services/profile.service';
import { LoadingService} from '../../../shared/services/loading.service';
import { ModalService } from '../../../shared/services/modal.service';
import { ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { element } from '@angular/core/src/render3/instructions';
import { TranslateService } from '@ngx-translate/core';
import { LocalizedDatePipe } from '../../../shared/localized-date.pipe';

@Component({
  selector: 'app-registration-checkout',
  templateUrl: './registration-checkout.component.html',
  styleUrls: ['./registration-checkout.component.scss']
})
export class RegistrationCheckoutComponent implements OnInit {
  @ViewChild('templateSuccess') successTemplate: TemplateRef<any>;
  @ViewChild('templateFail') failTemplate: TemplateRef<any>;

  private registration: any;
  private department: any;
  private coursesToRegister: any;
  private effectiveStatus: any;
  private code: any;
  private failedCourses: any = [];
  private totalEcts = 0;
  private totalUnits = 0;
  public loading = true;
  private openRegistrationPeriod = false;
  private isRegistrationPeriod: boolean;
  private isEmptyRegistration = false;

  // boolean to check if student has edited his registration so we can
  // show/hide the "Submit" button respectively
  private registrationEdited: boolean = false;

  private registrationPeriodStart: Date;
  private registrationPeriodEnd: Date;
  public start: string;
  public end: string;
  private modalRef: any;

  constructor(private currentRegService: CurrentRegistrationService,
              private route: ActivatedRoute,
              private profileService: ProfileService, 
              private loadingService: LoadingService,
              private _translateService: TranslateService,
              private modal: ModalService) {
  }

  ngOnInit() {
    this.loadingService.showLoading();  // show loading
    this.profileService.getStudent().then(student => {
      //get student department
      this.department = student.department;
      this.registrationPeriodStart = this.department.registrationPeriodStart;
      this.registrationPeriodEnd = this.department.registrationPeriodEnd;

      this.start = new LocalizedDatePipe(this._translateService).transform(this.registrationPeriodStart, 'shortDate');

      this.end = new LocalizedDatePipe(this._translateService).transform(this.registrationPeriodEnd, 'shortDate');
    
      this.currentRegService.getCurrentRegistration().then(currentReg => {
        //Perform a check to see if student changed the registration.
        //This is done by comparing the 2 arrays, so we need to encode the first
        //array into JSON(the second one already is encoded from session storage)
        //in order to be able to compare them.
        if(JSON.stringify(currentReg.classes) == sessionStorage["InitialRegistration"]){
          this.registrationEdited = false;
        }else{
          this.registrationEdited = true;
        }
        
        this.currentRegService.getAvailableClasses().then(availableClasses => {
          //If there are courses in the registration
          if(currentReg.classes.length > 0){
            currentReg.classes.forEach(x => {
              let availableClass = availableClasses.value.find(y => {
                  return y.courseClass == (x.courseClass ? x.courseClass.id || x.courseClass : x.courseClass);
              });
              if(availableClass){
                Object.assign(x, {
                  name: availableClass.name,
                  displayCode: availableClass.displayCode,
                  courseType: availableClass.courseType,
                  specialty: availableClass.specialty
                });
              }
            });

            this.registration = currentReg
            this.coursesToRegister = currentReg.classes;
            this.calculateTotal();
          }else{
            //if there are no courses in the registration this boolean will 
            //show a relevant message to the user
            this.isEmptyRegistration = true;
            this.coursesToRegister = [];
          }

          //Check wether we are between registration period deadlines
          this.isRegistrationPeriod = this.checkRegistrationPeriodDeadlines();

          this.currentRegService.getCurrentRegistrationEffectiveStatus().then(effectiveStatus => {
            this.effectiveStatus = effectiveStatus;
            this.code = this.effectiveStatus.code;
            if(this.effectiveStatus.status == "open") this.openRegistrationPeriod = true;

            //If student made changes in the registration, update the effective status
            //to equal the fake status "CLIENT_EDIT_REGISTRATION" in order to show the appropriate
            //edit messages at checkout and not the old effective status messages.
            if(this.registrationEdited && this.effectiveStatus.code != "OPEN_NO_TRANSACTION"){
              this.code = "CLIENT_EDIT_REGISTRATION";
            }
            this.loading = false; // Data is loaded
            this.loadingService.hideLoading(); // hide loading
          })
        })
      })
    })


  }

  registerSelected(){
    this.currentRegService.saveCurrentRegistration().then(currentRegistrationResult => {
      if(currentRegistrationResult.validationResult.code == "SUCC"){
        this.openModal(this.successTemplate)
      }else{
        this.openModal(this.failTemplate)
      }

      //If registration save was successfull, purge all session storage data
      if(currentRegistrationResult.validationResult){
        this.currentRegService.reset();
      }

      let registeredCourses = currentRegistrationResult.classes;
      registeredCourses.forEach(course => {
        if(!course.validationResult.success){
          this.failedCourses.push(course);
        }
      });
    });

  }

  checkRegistrationPeriodDeadlines(): boolean {
    var today = new Date();
    if(this.registrationPeriodStart < today && this.registrationPeriodEnd > today) return true;
    return false;
  }

  //function that sums the total units and ects of the courses to be registered
  calculateTotal(){
    if(this.coursesToRegister){
      for(let i=0; i<this.coursesToRegister.length; ++i){
        this.totalUnits += this.coursesToRegister[i].units;
        this.totalEcts += this.coursesToRegister[i].ects;
      }
    }
  }

  enableEdit(){
    sessionStorage.setItem("edit", "TRUE");
  }
  
  openModal(template: TemplateRef<any>){
    this.modalRef = this.modal.openModal(template);
  }

  closeModal(){
    // Reload the page. A more specific function may be implemented later
    this.ngOnInit()
    this.modalRef.hide();
  }

  backToEdit(){
    this.enableEdit();
    this.closeModal();
  }

}
